package javarogue.generation.algorithms;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javarogue.generation.rooms.Room;
import javarogue.level.Level;
import javarogue.level.LevelImpl;
import javarogue.player.Player;
import javarogue.tileengine.Tile;
import javarogue.utility.Matrix;
import javarogue.utility.Position;

/**
 * <h1>Generation</h1>
 * Simple generation algorithm, creates connected rooms and fills map with
 * objects.
 * 
 **/
public class SimpleGeneration implements GenerationAlgorithm {
	
	// -- Matrixes --
	private Matrix<Tile> tiles;
	private Matrix<Tile> objects;
	// -- Data Structures --
	private List<Room> rooms;
	// -- Parameters --
	private int roomNumber;
	private int size;
	// -- Seeded random number generator --
	private Random random;
	/**
	 * 
	 * Generates a dungeon of provided width and height with provided number of rooms.
	 * 
	 * @param width			Dungeon width.
	 * @param height		Dungeon height.
	 * @param roomNumber	Number of rooms.
	 */
	public SimpleGeneration(int size, int roomNumber, long seed) {
		this.rooms = new LinkedList<>();
		this.size = size;
		this.roomNumber = roomNumber;
		this.random = new Random();
		this.random.setSeed(seed);
	}
	
	@Override
	public Level generateLevel() {
		Level level = new LevelImpl();
		level.setTileMap(this.generateTiles());
		level.setObjectMap(this.generateObjects());
		Position entry = null;
		
		for(int i = 0; i < level.getObjectMap().getRows(); i++) {
			for(int j = 0; j < level.getObjectMap().getCols(); j++) {
				if(level.getObjectMap().get(i, j).equals(Tile.STAIRS_UP)) {
					entry = new Position(i, j);
				}
			}
		}
		level.setPlayer(new Player(entry));
		return level;
	}

	private Matrix<Tile> generateTiles() {
		SimpleGenerationTiles gen = new SimpleGenerationTiles(this.size, this.roomNumber, this.random);
		this.tiles = gen.generateTiles();
		this.rooms = gen.getRooms();
		return this.tiles;
	}

	private Matrix<Tile> generateObjects() {
		SimpleGenerationObjects gen = new SimpleGenerationObjects(this.size, this.tiles, this.rooms, this.random);
		this.objects = gen.generateObjects();
		return this.objects;
	}
}