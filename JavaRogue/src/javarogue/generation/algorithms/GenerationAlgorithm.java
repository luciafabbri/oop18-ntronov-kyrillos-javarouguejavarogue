package javarogue.generation.algorithms;

import javarogue.level.Level;

/**
 * <h1>GenerationAlgorithm</h1>
 * Algorithm for generation of rooms and objects in a level.
 *
 */
public interface GenerationAlgorithm {

	/**
	 * 
	 * @return Generated Level
	 */
	public Level generateLevel();
	
}
