package javarogue.generation.corridor;

import javarogue.pathfinding.PathFinderBFS;
import javarogue.pathfinding.PathFinderContext;
import javarogue.tileengine.Tile;
import javarogue.utility.Matrix;
import javarogue.utility.Position;

/**
 * <h1>CorridorImpl</h1>
 * 
 * A "simple" corridor using BFS.
 *
 */
public class CorridorImpl extends AbstractCorridor {

	/**
	 * Builds a corridor
	 * 
	 * @param origin Origin of the corridor.
	 * @param destination Destination if the corridor.
	 * @param map Tile map in which to generate the corridor
	 */
	public CorridorImpl(Position origin, Position destination, Matrix<Tile> map) {
		super(origin, destination, map);
	}

	/**
	 * Generate the corridor on the provided tile map using BFS.
	 */
	@Override
	public void generate() {
		// Simply find the shortestPath between origin and destination and draws it
		PathFinderContext pathFinder = new PathFinderContext(this.getMap());
		pathFinder.setPathFinder(new PathFinderBFS());
		this.setPath(pathFinder.findPath(this.getOrigin(), this.getDestination()).get());
		this.addTiles(this.getPath());
	}

}
