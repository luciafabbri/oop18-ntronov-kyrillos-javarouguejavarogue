package javarogue.generation.rooms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

import javarogue.generation.rooms.Room;
import javarogue.tileengine.Tile;
import javarogue.utility.Matrix;
import javarogue.utility.Position;

/**
 * <h1>AbstractRoom</h1>
 * 
 * A implementation of {@link Room}
 * @see Room
 *
 */
public abstract class AbstractRoom implements Room {

	private Matrix<Tile> map;
	private Position origin;
	private int width;
	private int height;
	private Map<Position, Tile> tiles;
	private List<Position> floor;

	/**
	 * Builds a new rectangle room.
	 * @param origin The top-right corner of the room.
	 * @param map The referenced tile map.
	 * @param generator The seeded random number generator.
	 */
	public AbstractRoom(Position origin, Matrix<Tile> map, Random generator) {
		tiles = new HashMap<>();
		this.map = map;
		this.origin = origin;
		this.width = 6 + generator.nextInt(3);
		this.height = 6 + generator.nextInt(3);
	}

	@Override
	public boolean canBeGenerated() {
		// Set collison flag to false.
		boolean collision = false;
		// Check if the room is in bounds.
		if (this.checkBounds()) {
			int originRow = origin.getX();
			int originCol = origin.getY();
			// Check if any candidate tile is occupied.
			for (int i = 0; i < this.height; i++) {
				for (int j = 0; j < this.width; j++) {
					// If everything is inBounds, check for collision.
					if (!collision) {
						collision = this.checkCollision(new Position(originRow + i, originCol + j));
					}
				}
			}
		} else {
			collision = true;
		}
		// Collsion is false if no collision / out of bounds was detected.
		return !collision;
	}

	@Override
	public Map<Position, Tile> getTileCoordinates() {
		return this.tiles;
	}
	
	@Override
	public List<Position> getFloor() {
		this.floor = this.mapFloor();
		return this.floor;
	}
	
	public Matrix<Tile> getMap() {
		return map;
	}

	public void setMap(Matrix<Tile> map) {
		this.map = map;
	}

	public Position getOrigin() {
		return origin;
	}

	public void setOrigin(Position origin) {
		this.origin = origin;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	private boolean checkCollision(Position pos) {
		//Simply check if selected tile is empty on the map
		if(this.map.get(pos.getX(), pos.getY()) != Tile.VOID) {
			return true;
		} else {
			return false;
		}
	}
	
	private boolean checkBounds() {
		// Set flag to true.
		boolean isInBounds = true;
		// Separate origin rows and cols.
		int originRow = origin.getX();
		int originCol = origin.getY();
		// Check if origin is in bounds (should be handled by the caller...).
		if(originRow < 0 || originRow >= this.map.getRows() || originCol < 0 || originCol >= this.map.getCols()) {
			isInBounds = false;
		} else {
			// If origin is in bounds, check if max height and max width are in bounds.
			int maxRow = origin.getX() + this.height - 1;
			int maxCol = origin.getY() + this.width - 1;
			if(maxRow < 0 || maxRow >= this.map.getRows() || maxCol < 0 || maxCol >= this.map.getCols()) {
				isInBounds = false;
			}
		}
		// Finally, return.
		return isInBounds;
	}
	
	private List<Position> mapFloor() {
		List<Position> floorTiles = new ArrayList<>();
		for (Entry<Position, Tile> e : this.tiles.entrySet()) {
			if(e.getValue() == Tile.FLOOR) {
				floorTiles.add(e.getKey());
			}
		}
		return floorTiles;
	}

}
