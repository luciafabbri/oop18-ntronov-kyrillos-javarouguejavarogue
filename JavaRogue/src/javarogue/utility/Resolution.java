package javarogue.utility;

/**
 * <h1>Resolution</h1>
 * 
 * Represents various resolutions of a window. Has a method to provide window
 * width and height. Overrides toString to represent resolution in a
 * conventional way.
 *
 */
public enum Resolution {

	HD(1366, 768),
	FHD(1920, 1080),
	WXGA(1280, 720);
	
	private int width;
	private int height;
	
	private Resolution(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	/**
	 * 
	 * @return resolution width in pixels
	 */
	public int getWidth() {
		return this.width;
	}
	
	/**
	 * 
	 * @return resolution height in pixels
	 */
	public int getHeight() {
		return this.height;
	}
	
	@Override
	public String toString() {
		return this.width + " x " + this.height;
	}
}
