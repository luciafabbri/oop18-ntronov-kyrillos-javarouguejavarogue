package javarogue.ui.config;

import java.util.List;
import java.util.Optional;

import javarogue.utility.Resolution;
import javarogue.utility.TilesetName;

/**
 * <h1>ConfigModel</h1>
 * 
 * Model of the Configuration Window, contains selected configuration options.
 *
 */
public interface ConfigModel {

	/**
	 * Saves resolution.
	 * 
	 * @param resolution
	 */
	public void setResolution(Resolution resolution);
	
	/**
	 * 
	 * Gets resolution if present.
	 * 
	 * @return Optional of resolution (empty if no selection has been made).
	 */
	public Optional<Resolution> getResolution();
	
	/**
	 * 
	 * @param isFullscreen Fullscreen option
	 */
	public void setFullscreen(boolean isFullscreen);
	
	/**
	 * 
	 * @return Fullscreen option
	 */
	public boolean getFullscreen();
	
	/**
	 * 
	 * @param name Tileset name
	 */
	public void setTileset(String name);
	
	/**
	 * 
	 * @return Tileset Name
	 */
	public Optional<String> getTileset();
	
	/**
	 * 
	 * @param seed Random number generation seed
	 */
	public void setSeed(long seed);
	
	/**
	 * 
	 * @return Random number generation seed
	 */
	public long getSeed();
	
	/**
	 * 
	 * @return Available tileset names
	 */
	public List<TilesetName> getTileSets();
	
	/**
	 * 
	 * @return Available resolutions
	 */
	public List<Resolution> getResolutions();
}
