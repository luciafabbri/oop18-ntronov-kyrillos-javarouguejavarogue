package javarogue.ui.game;

/**
 * <h1>GameView</h1>
 * 
 * View of the Game Window, presents the game visuals to the user.
 *
 */
public interface GameView {

	/**
	 * 
	 * Links Controller
	 * 
	 * @param controller
	 */
	public void setController(GameController controller);
	
	/**
	 *  Re-draws graphics, should be called after every game step (tick).
	 */
	public void render();
	
	/**
	 *  Opens the game UI.
	 */
	public void open();
	
	/**
	 *  Closes the game UI.
	 */
	public void close();
	
}
