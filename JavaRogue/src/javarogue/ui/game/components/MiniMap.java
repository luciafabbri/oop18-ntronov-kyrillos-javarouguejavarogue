package javarogue.ui.game.components;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javarogue.level.Level;
import javarogue.ui.game.GameViewImpl;
import javarogue.utility.Position;

/**
 * <h1>MiniMap</h1>
 * 
 * A GUI component that handles game's "minimap".
 * @see GameViewImpl
 *
 */
public class MiniMap {

	private GraphicsContext context;
	private Level level;

	/**
	 * 
	 * @param context Rendering context
	 */
	public MiniMap(GraphicsContext context) {
		this.context = context;
	}

	/**
	 * 
	 * @param level level to be linked with the minimap
	 */
	public void setLevel(Level level) {
		this.level = level;
	}

	/**
	 *  renders the camera data on the context.
	 */
	public void draw() {
		double scale = 8;
		double offsetCol = this.context.getCanvas().getWidth() - this.level.getTileMap().getCols() * scale;
		this.level.getTileMap().doubleFor((i, j) -> {
			switch (this.level.getTileMap().get(i, j)) {
			case FLOOR:
				this.context.setFill(MinimapColor.WHITE.getColor());
				this.context.fillRect(j * scale + offsetCol, i * scale, scale, scale);
				break;
			case WATER:
				this.context.setFill(MinimapColor.WATERBLUE.getColor());
				this.context.fillRect(j * scale + offsetCol, i * scale, scale, scale);
				break;
			case FLOOR_VAULT:
				this.context.setFill(MinimapColor.YELLOW.getColor());
				this.context.fillRect(j * scale + offsetCol, i * scale, scale, scale);
				break;
			default:
				break;
			}
			
			switch (this.level.getObjectMap().get(i, j)) {
			case STAIRS_DOWN:
				this.context.setFill(MinimapColor.RED.getColor());
				this.context.fillRect(j * scale + offsetCol, i * scale, scale, scale);
				break;
			case STAIRS_UP:
				this.context.setFill(MinimapColor.BLUE.getColor());
				this.context.fillRect(j * scale + offsetCol, i * scale, scale, scale);
				break;
			default:
				break;
			}
		});
		
		Position playerPos = this.level.getPlayerPos();
		this.context.setFill(MinimapColor.GREEN.getColor());
		this.context.fillRect(playerPos.getY() * scale + offsetCol, playerPos.getX() * scale, scale, scale);
	}
	
	private enum MinimapColor {
		
		WHITE(225, 225, 225),
		WATERBLUE(100, 100, 225),
		YELLOW(225, 225, 100),
		RED(225, 0, 0),
		BLUE(0, 0, 255),
		GREEN(0, 255, 0);
		
		private Color color;
		
		private MinimapColor(int red, int green, int blue) {
			this.color = Color.rgb(red, green, blue, 0.5);
		}
		
		public Color getColor() {
			return this.color;
		}
	}

}
