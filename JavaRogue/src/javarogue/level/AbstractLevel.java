package javarogue.level;

import javarogue.player.Player;
import javarogue.tileengine.Tile;
import javarogue.ui.game.GameController;
import javarogue.utility.Matrix;
import javarogue.utility.Position;

/**
 * <h1>AbstractLevel</h1>
 * 
 * Abstract class to be used as a {@link Level} framework.
 *
 */
public abstract class AbstractLevel implements Level {

	private Matrix<Tile> tileMap;
	private Matrix<Tile> objectMap;
	
	private GameController controller;
	
	private Player player;
	
	@Override
	public Matrix<Tile> getTileMap() {
		return this.tileMap;
	}

	@Override
	public Matrix<Tile> getObjectMap() {
		return this.objectMap;
	}

	@Override
	public Position getPlayerPos() {
		return this.player.getPosition();
	}

	@Override
	public void setTileMap(Matrix<Tile> map){
		this.tileMap = map;
	}
	
	@Override
	public void setObjectMap(Matrix<Tile> map){
		this.objectMap = map;
	}
	
	@Override
	public void setPlayer(Player player) {
		this.player = player;
	}
	
	@Override
	public void setController(GameController controller) {
		this.controller = controller;
	}
	
	public GameController getController() {
		return this.controller;
	}
	
	public Player getPlayer() {
		return this.player;
	}
}
