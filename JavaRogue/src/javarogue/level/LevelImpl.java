package javarogue.level;

import java.util.Random;

import javarogue.tileengine.Tile;
import javarogue.utility.Position;
import javarogue.utility.Translation;

/**
 * <h1>LevelImpl</h1>
 *
 * Simple Extension of {@link AbstractLevel} which is an implementation of
 * {@link Level}
 */
public class LevelImpl extends AbstractLevel {

	@Override
	public void moveCharacter(Translation trans) {
		Position newPos = this.getPlayerPos().clone();
		newPos.translate(trans.getDx(), trans.getDy());
		if (newPos.getX() >= 0 && newPos.getY() >= 0 && newPos.getX() < this.getTileMap().getRows()
				&& newPos.getY() < this.getTileMap().getCols()) {
			Tile newTile = this.getTileMap().get(newPos.getX(), newPos.getY());
			switch (newTile) {
			case FLOOR:
			case FLOOR_VAULT:
				this.getPlayer().move(trans.getDx(), trans.getDy());
				break;
			case WATER:
				int roll = new Random().nextInt(3);
				if (roll != 0) {
					this.getPlayer().move(trans.getDx(), trans.getDy());
				}
				break;
			default:
				break;
			}
			Tile newObject = this.getObjectMap().get(newPos.getX(), newPos.getY());
			switch (newObject) {
			case STAIRS_UP:
				this.getController().changeLevel(this.getController().getCurrentDepth() - 1);
				break;
			case STAIRS_DOWN:
				this.getController().changeLevel(this.getController().getCurrentDepth() + 1);
				break;
			default:
				break;
			}
		}
	}
	
}
