package javarogue.level;

import javarogue.player.Player;
import javarogue.tileengine.Tile;
import javarogue.ui.game.GameController;
import javarogue.utility.Matrix;
import javarogue.utility.Position;
import javarogue.utility.Translation;

/**
 * <h1>Level</h1>
 * A game level with associated data and data handling.
 *
 */
public interface Level {

	/**
	 * 
	 * @return tile map
	 */
	public Matrix<Tile> getTileMap();
	
	/**
	 * 
	 * @param map tile map
	 */
	public void setTileMap(Matrix<Tile> map);

	/**
	 * 
	 * @return Level objects
	 */
	public Matrix<Tile> getObjectMap();
	
	/**
	 * 
	 * @param object map
	 */
	public void setObjectMap(Matrix<Tile> objects);

	/**
	 * 
	 * @return {@link Position} of player
	 * @see Player
	 */
	public Position getPlayerPos();
	
	/**
	 * 
	 * @param player {@link Player}
	 */
	public void setPlayer(Player player);
	
	/**
	 * Signal the player to be moved via {@link Translation}.
	 * @param trans Translation to be used to move the player
	 */
	public void moveCharacter(Translation trans);
	
	/**
	 * 
	 * @param controller link Controller for collision actions.
	 */
	public void setController(GameController controller);
	
}
